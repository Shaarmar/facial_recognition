from cv2 import *

names = []
cam = VideoCapture(0)   # 0 -> index of camera
s, img = cam.read()

def savePic(name):
    if s:    # frame cai)ptured without any errors
        namedWindow("cam-test")
        imshow("cam-test",img)
        k = waitKey(1)
        if k%256 == 27:
            print("... Closing")
        elif k%256 == 32:
            fileName = name + '.jpg'
            imwrite(fileName,img) #save image
            #cv2.destroyAllWindows()

def makeFile():
    while True:
        name = input("What is your name? ")
        savePic(name)
        names.append(name)
        q = input("Is that all? ")
        if q == "yes":
            break
        else:
            continue



cam.release()
makeFile()
cv2.destroyAllWindows()


